package com.epam.olevolo;

import java.io.File;
import java.io.IOException;

/**
 * @author Volodymyr Oleksiuk
 * @subject JUnit testing
 * @version 1.0 Nov 2018
 */

public class SampleClass {
    public File file;

    /** parsing number to string*/
    public int getNumber(String line) {
        try {
            int n = Integer.parseInt(line);
            return n;
        } catch (Exception e) {
            System.out.println("It's not a number..very bad. Returning -1");
            return -1;
        }
    }

    public void fileCreate() throws IOException {
        file = new File("data.txt");
        file.createNewFile();
    }
}
