package com.epam.olevolo;

import java.io.IOException;

/**
 * @author Volodymyr Oleksiuk
 * @subject JUnit testing
 * @version 1.0 Nov 2018
 */

public class SampleClassManager {

    private SampleClass sampleClass;

    public SampleClassManager(SampleClass sampleClass) {
        this.sampleClass = sampleClass;
    }

    public int getNumber(String line) {
        return sampleClass.getNumber(line);
    }

    public void fileCreate() throws IOException {
        sampleClass.fileCreate();
    }
}
