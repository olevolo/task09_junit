package com.epam.olevolo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SampleClassManagerTest.class
})
public class AllTests {
}